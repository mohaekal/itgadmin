import React from 'react';
import './App.css';
import ItgMainContent from './components/maincontent';
import "./shards-dashboard/styles/shards-dashboards.1.1.0.css";


class App extends React.Component {
  render() {
    return (

      <div className="menu-position-side menu-side-left full-screen with-content-panel" >
        <div className="all-wrapper with-side-panel solid-bg-all">
          <ItgMainContent />
        </div>
      </div>

    );
  }
}



export default App;


