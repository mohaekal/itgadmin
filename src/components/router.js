import React from 'react'
import { Switch, Route ,Redirect} from 'react-router-dom'
import AssignTask from './assigntask';
import ChartITGbar from './chartbar';
import CustomerProjects from './customerprojects';
import ProjectList from './projectlist';
import RecentOrder from './recentorder';
import SalesDash from './salesdash';
import ItgLogin from './loginITG';
import App from '.././App';
import page404 from './404component';
import ComponentsOverview from './ComponentsOverview';
import Info from './info';
// import DataTable from './datatable';

 
const Content = () =>{
  return(
    <Switch>
      <Route path="/app" component={App}/>
      <Route path="/content1" component={AssignTask}/>
      <Route path="/content7" component={Info}/>
      <Route path="/content3" component={CustomerProjects}/>
      <Route path="/content4" component={ProjectList}/>
      <Route path="/content5" component={RecentOrder}/>
      <Route path="/content6" component={SalesDash}/>
      <Route path="/login" component={ItgLogin} /> 
      <Route path='/404' component={page404} />
      <Route path='/content2' component={ComponentsOverview} />
      <Redirect from='*' to='/404' />
      {/* <Route path="/content7" component={DataTable}/> */}

    </Switch>
  )
}
 
export default Content;