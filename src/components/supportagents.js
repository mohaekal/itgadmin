import React from 'react';

const SupportAgents=() =>(


    <div className="element-wrapper">
    <h6 className="element-header">
      Support Agents
    </h6>
    <div className="element-box-tp">
      <div className="profile-tile">
        <a className="profile-tile-box" href="users_profile_small.html">
          <div className="pt-avatar-w">
            <img alt="" src={require(".././img/avatar1.jpg")} />
          </div>
          <div className="pt-user-name">
            John Mayers
          </div>
        </a>
        <div className="profile-tile-meta">
          <ul>
            <li>
              Last Login:<strong>Online Now</strong>
            </li>
            <li>
              Tickets:<strong><a href="apps_support_index.html">12</a></strong>
            </li>
            <li>
              Response Time:<strong>2 hours</strong>
            </li>
          </ul>
          <div className="pt-btn">
            <a className="btn btn-success btn-sm" href="apps_full_chat.html">Send Message</a>
          </div>
        </div>
      </div>
      <div className="profile-tile">
        <a className="profile-tile-box" href="users_profile_small.html">
          <div className="pt-avatar-w">
            <img alt="" src={require(".././img/avatar3.jpg")} />
          </div>
          <div className="pt-user-name">
            Ben Gossman
          </div>
        </a>
        <div className="profile-tile-meta">
          <ul>
            <li>
              Last Login:<strong>Offline</strong>
            </li>
            <li>
              Tickets:<strong><a href="apps_support_index.html">9</a></strong>
            </li>
            <li>
              Response Time:<strong>3 hours</strong>
            </li>
          </ul>
          <div className="pt-btn">
            <a className="btn btn-secondary btn-sm" href="apps_full_chat.html">Send Message</a>
          </div>
        </div>
      </div>
    </div>
  </div>


  
);

export default SupportAgents;