import React, {Component} from 'react';
import {Bar,Line,Pie,Doughnut} from 'react-chartjs-2'

class ChartITGX extends Component {
constructor(props){
super(props);
this.state= {
  chartData:{
    
      labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
      datasets: [{ 
          data: [86,114,106,106,107,111,133,221,783,2478],
          label: "Africa",
          borderColor: "#5797fc",
          fill: false
        }, { 
          data: [282,350,411,502,635,809,947,1402,3700,5267],
          label: "Asia",
          borderColor: "#7e6fff",
          fill: false
        }, { 
          data: [168,170,178,190,203,276,408,547,675,734],
          label: "Europe",
          borderColor: "#4ecc48",
          fill: false
        }, { 
          data: [40,20,10,16,24,38,74,167,508,784], 
          label: "Latin America",
          borderColor: "#ffcc29",
          fill: false
        }, { 
          data: [6,3,2,2,7,26,82,172,312,433],
          label: "North America",
          borderColor: "#f37070",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'World population per region (in millions)'
      }
    }
  }
}

render() {
return (
  <div className="ChartITG">
  <Line data={this.state.chartData}
  options={{
    maintainAspectRatio:false
  }} />


  </div>
)

}

}

export default ChartITGX;