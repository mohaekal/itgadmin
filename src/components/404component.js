import React from 'react';

const page404=() =>(

    <div className="content-box">
      <div className="big-error-w">
        <h1>
          404
        </h1>
        <h5>
          Page not Found
        </h5>
        <h4>
          Oops, Something went missing...
        </h4>
        <form>
          <div className="input-group">
            <input className="form-control" placeholder="Enter your search query here" type="text" />
            <br />
            <div className="input-group-btn" style={{paddingLeft: '5px'}}>
              <button className="btn btn-primary">Search</button>
            </div>
          </div>
        </form>
      </div>
    
 </div>



  
);

export default page404;