import React from 'react';

const TeamMembers=() =>(

    
    <div className="element-wrapper">
    <h6 className="element-header">
      Team Members
    </h6>
    <div className="element-box-tp">
      <div className="input-search-w">
        <input className="form-control rounded bright" placeholder="Search team members..." type="search" />
      </div>
      <div className="users-list-w">
        <div className="user-w with-status status-green">
          <div className="user-avatar-w">
            <div className="user-avatar">
              <img alt="" src="img/avatar1.jpg" />
            </div>
          </div>
          <div className="user-name">
            <h6 className="user-title">
              John Mayers
            </h6>
            <div className="user-role">
              Account Manager
            </div>
          </div>
          <a className="user-action" href="users_profile_small.html">
            <div className="os-icon os-icon-email-forward"></div>
          </a>
        </div>
        <div className="user-w with-status status-green">
          <div className="user-avatar-w">
            <div className="user-avatar">
              <img alt="" src="img/avatar2.jpg" />
            </div>
          </div>
          <div className="user-name">
            <h6 className="user-title">
              Ben Gossman
            </h6>
            <div className="user-role">
              Administrator
            </div>
          </div>
          <a className="user-action" href="users_profile_small.html">
            <div className="os-icon os-icon-email-forward"></div>
          </a>
        </div>
        <div className="user-w with-status status-red">
          <div className="user-avatar-w">
            <div className="user-avatar">
              <img alt="" src="img/avatar3.jpg" />
            </div>
          </div>
          <div className="user-name">
            <h6 className="user-title">
              Phil Nokorin
            </h6>
            <div className="user-role">
              HR Manger
            </div>
          </div>
          <a className="user-action" href="users_profile_small.html">
            <div className="os-icon os-icon-email-forward"></div>
          </a>
        </div>
        <div className="user-w with-status status-green">
          <div className="user-avatar-w">
            <div className="user-avatar">
              <img alt="" src="img/avatar4.jpg" />
            </div>
          </div>
          <div className="user-name">
            <h6 className="user-title">
              Jenny Miksa
            </h6>
            <div className="user-role">
              Lead Developer
            </div>
          </div>
          <a className="user-action" href="users_profile_small.html">
            <div className="os-icon os-icon-email-forward"></div>
          </a>
        </div>
      </div>
    </div>
  </div>


  
);

export default TeamMembers;