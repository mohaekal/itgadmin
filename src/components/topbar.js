import React from 'react';

const TopBar=() =>(

    
    <div className="top-bar color-scheme-transparent">

    <div className="top-menu-controls">
      <div className="element-search autosuggest-search-activator">
        <input placeholder="Start typing to search..." type="text" />
      </div>

      <div className="messages-notifications os-dropdown-trigger os-dropdown-position-left">
        <i className="os-icon os-icon-mail-14"></i>
        <div className="new-messages-count">
          12
        </div>
        <div className="os-dropdown light message-list">
          <ul>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar1.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    John Mayers
                  </h6>
                  <h6 className="message-title">
                    Account Update
                  </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar2.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Phil Jones
                  </h6>
                  <h6 className="message-title">
                    Secutiry Updates
                  </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar3.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Bekky Simpson
                  </h6>
                  <h6 className="message-title">
                    Vacation Rentals
                  </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar4.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Alice Priskon
                  </h6>
                  <h6 className="message-title">
                    Payment Confirmation
                  </h6>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div className="top-icon top-settings os-dropdown-trigger os-dropdown-position-left">
        <i className="os-icon os-icon-ui-46"></i>
        <div className="os-dropdown">
          <div className="icon-w">
            <i className="os-icon os-icon-ui-46"></i>
          </div>
          <ul>
            <li>
              <a href="users_profile_small.html"><i className="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
            </li>
            <li>
              <a href="users_profile_small.html"><i className="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
            </li>
            <li>
              <a href="users_profile_small.html"><i className="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
            </li>
            <li>
              <a href="users_profile_small.html"><i className="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
            </li>
          </ul>
        </div>
      </div>

      <div className="logged-user-w">
        <div className="logged-user-i">
          <div className="avatar-w">
            <img alt="" src={require(".././img/avatar1.jpg")} />
          </div>
          <div className="logged-user-menu color-style-bright">
            <div className="logged-user-avatar-info">
              <div className="avatar-w">
                <img alt="" src={require(".././img/avatar1.jpg")} />
              </div>
              <div className="logged-user-info-w">
                <div className="logged-user-name">
                  Maria Gomez
                </div>
                <div className="logged-user-role">
                  Administrator
                </div>
              </div>
            </div>
            <div className="bg-icon">
              <i className="os-icon os-icon-wallet-loaded"></i>
            </div>
            <ul>
              <li>
                <a href="apps_email.html"><i className="os-icon os-icon-mail-01"></i><span>Incoming Maillll</span></a>
              </li>
              <li>
                <a href="users_profile_big.html"><i className="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
              </li>
              <li>
                <a href="users_profile_small.html"><i className="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
              </li>
              <li>
                <a href="#"><i className="os-icon os-icon-others-43"></i><span>Notifications</span></a>
              </li>
              <li>
                <a href="#"><i className="os-icon os-icon-signs-11"></i><span>Logout</span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
 
    </div>

  </div>

  
);

export default TopBar;