import React from 'react';
import ChartITG from '.././components/chart';
const ChartITGbar = () => (


  <div className="row">
    <div className="col-sm-12 col-xxxl-9">
      <div className="element-wrapper">
        <h6 className="element-header">
          Unique Visitors Graph
        </h6>
        <div className="element-box">
          <div class="chart-container" style={{ position: 'relative', height: '150px', width: '99%' }}>
            <ChartITG />
          </div>
        </div>
      </div>
    </div>
    <div className="d-none d-xxxl-block col-xxxl-3">
      <div className="element-wrapper">
        <h6 className="element-header">
          Visitors by Browser
        </h6>
        <div className="element-box less-padding">
          <div className="el-chart-w">
            <canvas height="120" id="donutChart1" width="120"></canvas>
            <div className="inside-donut-chart-label">
              <strong>1,248</strong><span>Visitors</span>
            </div>
          </div>
          <div className="el-legend condensed">
            <div className="row">
              <div className="col-auto col-xxxxl-6 ml-sm-auto mr-sm-auto">
                <div className="legend-value-w">
                  <div className="legend-pin legend-pin-squared" style={{ backgroundColor: '#6896f9' }}></div>
                  <div className="legend-value">
                    <span>Safari</span>
                    <div className="legend-sub-value">
                      17%, 12 Visits
                    </div>
                  </div>
                </div>
                <div className="legend-value-w">
                  <div className="legend-pin legend-pin-squared" style={{ backgroundColor: '#85c751' }}></div>
                  <div className="legend-value">
                    <span>Chrome</span>
                    <div className="legend-sub-value">
                      22%, 763 Visits
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 d-none d-xxxxl-block">
                <div className="legend-value-w">
                  <div className="legend-pin legend-pin-squared" style={{ backgroundColor: '#806ef9' }}></div>
                  <div className="legend-value">
                    <span>Firefox</span>
                    <div className="legend-sub-value">
                      3%, 7 Visits
                    </div>
                  </div>
                </div>
                <div className="legend-value-w">
                  <div className="legend-pin legend-pin-squared" style={{ backgroundColor: '#d97b70' }}></div>
                  <div className="legend-value">
                    <span>Explorer</span>
                    <div className="legend-sub-value">
                      15%, 45 Visits
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>



);

export default ChartITGbar;