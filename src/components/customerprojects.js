import React from 'react';
import Background1 from '.././img/portfolio9.jpg';
import Background2 from '.././img/portfolio2.jpg';
import Background3 from '.././img/portfolio12.jpg';
import Background4 from '.././img/portfolio4.jpg';
import Background5 from '.././img/portfolio8.jpg';

const CustomerProjects=() =>(


    <div className="row">
    <div className="col-sm-12 col-xxxl-6">
      <div className="element-wrapper">
        <h6 className="element-header">
          New Orders
        </h6>
        <div className="element-box">
          <div className="table-responsive">
            <table className="table table-lightborder">
              <thead>
                <tr>
                  <th>
                    Customer
                  </th>
                  <th>
                    Products
                  </th>
                  <th className="text-center">
                    Status
                  </th>
                  <th className="text-right">
                    Total
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="nowrap">
                    John Mayers
                  </td>
                  <td>
                    <div className="cell-image-list">
                      <div className="cell-img" style={{backgroundImage: "url(" + Background1 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background2 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background3 + ")"}} ></div>
                      <div className="cell-img-more">
                        + 5 more
                      </div>
                    </div>
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="text-right">
                    $354
                  </td>
                </tr>
                <tr>
                  <td className="nowrap">
                    Kelly Brans
                  </td>
                  <td>
                    <div className="cell-image-list">
                      <div className="cell-img" style={{backgroundImage: "url(" + Background4 + ")"}}></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background5 + ")"}} ></div>
                    </div>
                  </td>
                  <td className="text-center">
                    <div className="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                  </td>
                  <td className="text-right">
                    $94
                  </td>
                </tr>
                <tr>
                  <td className="nowrap">
                    Tim Howard
                  </td>
                  <td>
                    <div className="cell-image-list">
                    <div className="cell-img" style={{backgroundImage: "url(" + Background1 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background2 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background3 + ")"}} ></div>
                    </div>
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="text-right">
                    $156
                  </td>
                </tr>
                <tr>
                  <td className="nowrap">
                    Joe Trulli
                  </td>
                  <td>
                    <div className="cell-image-list">
                    <div className="cell-img" style={{backgroundImage: "url(" + Background2 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background4 + ")"}} ></div>
                      <div className="cell-img" style={{backgroundImage: "url(" + Background1 + ")"}} ></div>
                      <div className="cell-img-more">
                        + 2 more
                      </div>
                    </div>
                  </td>
                  <td className="text-center">
                    <div className="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                  </td>
                  <td className="text-right">
                    $1,120
                  </td>
                </tr>
                <tr>
                  <td className="nowrap">
                    Jerry Lingard
                  </td>
                  <td>
                    <div className="cell-image-list">
                    <div className="cell-img" style={{backgroundImage: "url(" + Background5 + ")"}} ></div>
                    </div>
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="text-right">
                    $856
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
     <div className="d-none d-xxxl-block col-xxxl-6">
      {/* <!--START - Questions per Product--> */}
      <div className="element-wrapper">
        <div className="element-actions">
          <form className="form-inline justify-content-sm-end">
            <select className="form-control form-control-sm rounded">
              <option value="Pending">
                Today
              </option>
              <option value="Active">
                Last Week 
              </option>
              <option value="Cancelled">
                Last 30 Days
              </option>
            </select>
          </form>
        </div>
        <h6 className="element-header">
          Inventory Stats
        </h6>
        <div className="element-box">
          <div className="os-progress-bar primary">
            <div className="bar-labels">
              <div className="bar-label-left">
                <span className="bigger">Eyeglasses</span>
              </div>
              <div className="bar-label-right">
                <span className="info">25 items / 10 remaining</span>
              </div>
            </div>
            <div className="bar-level-1" style={{width: '100%'}}>
              <div className="bar-level-2" style={{width: '70%'}}>
                <div className="bar-level-3" style={{width: '40%'}}></div>
              </div>
            </div>
          </div>
          <div className="os-progress-bar primary">
            <div className="bar-labels">
              <div className="bar-label-left">
                <span className="bigger">Outwear</span>
              </div>
              <div className="bar-label-right">
                <span className="info">18 items / 7 remaining</span>
              </div>
            </div>
            <div className="bar-level-1" style={{width: '100%'}}>
              <div className="bar-level-2" style={{width: '40%'}}>
                <div className="bar-level-3" style={{width: '20%'}}></div>
              </div>
            </div>
          </div>
          <div className="os-progress-bar primary">
            <div className="bar-labels">
              <div className="bar-label-left">
                <span className="bigger">Shoes</span>
              </div>
              <div className="bar-label-right">
                <span className="info">15 items / 12 remaining</span>
              </div>
            </div>
            <div className="bar-level-1" style={{width: '100%'}}>
              <div className="bar-level-2" style={{width: '60%'}}>
                <div className="bar-level-3" style={{width: '30%'}}></div>
              </div>
            </div>
          </div>
          <div className="os-progress-bar primary">
            <div className="bar-labels">
              <div className="bar-label-left">
                <span className="bigger">Jeans</span>
              </div>
              <div className="bar-label-right">
                <span className="info">12 items / 4 remaining</span>
              </div>
            </div>
            <div className="bar-level-1" style={{width: '100%'}}>
              <div className="bar-level-2" style={{width: '30%'}}>
                <div className="bar-level-3" style={{width: '10%'}}></div>
              </div>
            </div>
          </div>
          <div className="mt-4 border-top pt-3">
            <div className="element-actions d-none d-sm-block">
              <form className="form-inline justify-content-sm-end">
                <select className="form-control form-control-sm form-control-faded">
                  <option selected={true}>
                    Last 30 days
                  </option>
                  <option>
                    This Week
                  </option>
                  <option>
                    This Month
                  </option>
                  <option>
                    Today
                  </option>
                </select>
              </form>
            </div>
            <h6 className="element-box-header">
              Inventory History
            </h6>
            <div className="el-chart-w">
              <canvas data-chart-data="13,28,19,24,43,49,40,35,42,46,38,32,45" height="50" id="liteLineChartV3" width="300"></canvas>
            </div>
          </div>
        </div>
      </div>
      {/* <!--END - Questions per product                  --> */}
    </div>
  </div>


  
);

export default CustomerProjects;