import React from 'react';
import BreadCumb from '.././components/breadcumb';
import ContentPanelToogler from '.././components/contentpaneltoogler';
import MainMenu from '.././components/mainmenu';
import MobileMenu from '.././components/mobilemenu';
import TopBar from '.././components/topbar';
import Content from './router'

const ItgMainContent = () => (
    <div className="layout-w">
        <MobileMenu />
        <MainMenu />
        <div className="content-w">
            <TopBar />
            <BreadCumb />
            <ContentPanelToogler />
            <div className="content-i">
                <div className="content-box">
                    {/* <SalesDash />
                    <ProjectList />
                    <ChartITGbar />
                    <CustomerProjects />
                    <RecentOrder />
                    <AssignTask /> */}
                    <Content />
                </div>

                {/* <div className="content-panel">
                    <div className="content-panel-close">
                        <i className="os-icon os-icon-close"></i>
                    </div>
                    <QuickLinks />
                    <SupportAgents />
                    <RecentActivity />
                    <TeamMembers />
                </div> */}

            </div>
        </div>

    </div>




);

export default ItgMainContent;