import React from 'react';
import { Link } from 'react-router-dom';


const MainMenu = () => (

  <div className="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
    <div className="logo-w">
      <Link to="/"><img src={require(".././img/logo.png")} />

        <div className="logo-label">
          ITG Admin
             </div>
      </Link>
    </div>
    <div className="logged-user-w avatar-inline">
      <div className="logged-user-i">
        <div className="avatar-w">
          <img alt="" src={require(".././img/avatar1.jpg")} />
        </div>
        <div className="logged-user-info-w">
          <div className="logged-user-name">
            Mohamad Haekal
               </div>
          <div className="logged-user-role">
            Administrator
               </div>
        </div>
        <div className="logged-user-toggler-arrow">
          <div className="os-icon os-icon-chevron-down"></div>
        </div>
        <div className="logged-user-menu color-style-bright">
          <div className="logged-user-avatar-info">
            <div className="avatar-w">
              <img alt="" src={require(".././img/avatar1.jpg")} />
            </div>
            <div className="logged-user-info-w">
              <div className="logged-user-name">
                Mohamad Haekal
                   </div>
              <div className="logged-user-role">
                Administrator
                   </div>
            </div>
          </div>
          <div className="bg-icon">
            <i className="os-icon os-icon-wallet-loaded"></i>
          </div>
          <ul>
            <li>
              <a href="apps_email "><i className="os-icon os-icon-mail-01"></i><span>Incoming Maill</span></a>
            </li>
            <li>
              <a href="users_profile_big "><i className="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
            </li>
            <li>
              <a href="users_profile_small "><i className="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
            </li>
            <li>
              <a href="#"><i className="os-icon os-icon-others-43"></i><span>Notifications</span></a>
            </li>
            <li>
              <a href="#"><i className="os-icon os-icon-signs-11"></i><span>Logout</span></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div className="menu-actions">

      <div className="messages-notifications os-dropdown-trigger os-dropdown-position-right">
        <i className="os-icon os-icon-mail-14"></i>
        <div className="new-messages-count">
          12
             </div>
        <div className="os-dropdown light message-list">
          <ul>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar1.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    John Mayers
                       </h6>
                  <h6 className="message-title">
                    Account Update
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar2.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Phil Jones
                       </h6>
                  <h6 className="message-title">
                    Secutiry Updates
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar3.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Bekky Simpson
                       </h6>
                  <h6 className="message-title">
                    Vacation Rentals
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar4.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Alice Priskon
                       </h6>
                  <h6 className="message-title">
                    Payment Confirmation
                       </h6>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
      {/* <!--------------------
           END - Messages Link in secondary top menu
           --------------------><!--------------------
           START - Settings Link in secondary top menu
           --------------------> */}
      <div className="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">
        <i className="os-icon os-icon-ui-46"></i>
        <div className="os-dropdown">
          <div className="icon-w">
            <i className="os-icon os-icon-ui-46"></i>
          </div>
          <ul>
            <li>
              <a href="users_profile_small "><i className="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
            </li>
            <li>
              <a href="users_profile_small "><i className="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
            </li>
            <li>
              <a href="users_profile_small "><i className="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
            </li>
            <li>
              <a href="users_profile_small "><i className="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
            </li>
          </ul>
        </div>
      </div>
      {/* <!--------------------
           END - Settings Link in secondary top menu
           --------------------><!--------------------
           START - Messages Link in secondary top menu
           --------------------> */}
      <div className="messages-notifications os-dropdown-trigger os-dropdown-position-right">
        <i className="os-icon os-icon-zap"></i>
        <div className="new-messages-count">
          4
             </div>
        <div className="os-dropdown light message-list">
          <div className="icon-w">
            <i className="os-icon os-icon-zap"></i>
          </div>
          <ul>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar1.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    John Mayers
                       </h6>
                  <h6 className="message-title">
                    Account Update
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar2.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Phil Jones
                       </h6>
                  <h6 className="message-title">
                    Secutiry Updates
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar3.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Bekky Simpson
                       </h6>
                  <h6 className="message-title">
                    Vacation Rentals
                       </h6>
                </div>
              </a>
            </li>
            <li>
              <a href="#">
                <div className="user-avatar-w">
                  <img alt="" src={require(".././img/avatar4.jpg")} />
                </div>
                <div className="message-content">
                  <h6 className="message-from">
                    Alice Priskon
                       </h6>
                  <h6 className="message-title">
                    Payment Confirmation
                       </h6>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
      {/* <!--------------------
           END - Messages Link in secondary top menu
           --------------------> */}
    </div>
    <div className="element-search autosuggest-search-activator">
      <input placeholder="Start typing to search..." type="text" />
    </div>
    <h1 className="menu-page-header">
      Page Header
         </h1>
    <ul className="main-menu">
      <li className="sub-header">
        <span>Header 1</span>
      </li>
      <li className="selected has-sub-menu">
        <a href="/">
          <div className="icon-w">
            <div className="os-icon os-icon-layout"></div>
          </div>
          <span>Dashboard</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Dashboard
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-layout"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <Link to="/content1">Content 1</Link>
              </li>
              <li>
                <Link to="/content2">Content 2</Link>
              </li>
              <li>
                <Link to="/content3">Content 3</Link>
              </li>
              <li>
                <Link to="/content4">Content 4</Link>
              </li>
              <li>
                <Link to="/content5">Content 5</Link>
              </li>
              <li>
                <Link to="/content6">Content 6</Link>
              </li>
              <li>
                <Link to="/content7">Content 7</Link>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="layouts_menu_top_image ">
          <div className="icon-w">
            <div className="os-icon os-icon-layers"></div>
          </div>
          <span>Menu 1</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Menu Styles
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-layers"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <Link to="/content01">Content 1</Link>
              </li>
              <li>
                <a href="layouts_menu_side_full_dark ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_side_transparent ">Sub Menu <strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="apps_pipeline ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_projects ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_side_mini ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="layouts_menu_side_mini_dark ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_side_compact ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_side_compact_dark ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_right ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_top ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_top_dark ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="layouts_menu_top_image ">Sub Menu <strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="layouts_menu_sub_style_flyout ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_sub_style_flyout_dark ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_sub_style_flyout_bright ">Sub Menu</a>
              </li>
              <li>
                <a href="layouts_menu_side_compact_click ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className="sub-header">
        <span>Header 2</span>
      </li>
      <li className=" has-sub-menu">
        <a href="apps_bank ">
          <div className="icon-w">
            <div className="os-icon os-icon-package"></div>
          </div>
          <span>Menu 1</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Sub Menu
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-package"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="apps_email ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_support_dashboard ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_support_index ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_crypto ">Sub Menu <strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="apps_projects ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_bank ">Sub Menu<strong className="badge badge-danger">New</strong></a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="apps_full_chat ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_todo ">Sub Menu<strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="misc_chat ">Sub Menu</a>
              </li>
              <li>
                <a href="apps_pipeline ">Sub Menu</a>
              </li>
              <li>
                <a href="rentals_index_grid ">Sub Menu<strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="misc_calendar ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-file-text"></div>
          </div>
          <span>Menu 2</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Pages
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-file-text"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="misc_invoice ">Sub Menu</a>
              </li>
              <li>
                <a href="rentals_index_grid ">Sub Menu<strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="misc_charts ">Sub Menu</a>
              </li>
              <li>
                <a href="auth_login ">Sub Menu</a>
              </li>
              <li>
                <a href="auth_register ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="auth_lock ">Sub Menu</a>
              </li>
              <li>
                <a href="misc_pricing_plans ">Sub Menu</a>
              </li>
              <li>
                <a href="misc_error_404 ">Sub Menu</a>
              </li>
              <li>
                <a href="misc_error_500 ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-life-buoy"></div>
          </div>
          <span>Menu 3</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            UI Kit
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-life-buoy"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="uikit_modals ">Sub Menu<strong className="badge badge-danger">New</strong></a>
              </li>
              <li>
                <a href="uikit_alerts ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_grid ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_progress ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_popovers ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="uikit_tooltips ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_buttons ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_dropdowns ">Sub Menu</a>
              </li>
              <li>
                <a href="uikit_typography ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className="sub-header">
        <span>Header 3</span>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-mail"></div>
          </div>
          <span>Menu 1</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Emails
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-mail"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="emails_welcome ">Sub Menu</a>
              </li>
              <li>
                <a href="emails_order ">Sub Menu</a>
              </li>
              <li>
                <a href="emails_payment_due ">Sub Menu</a>
              </li>
              <li>
                <a href="emails_forgot ">Sub Menu</a>
              </li>
              <li>
                <a href="emails_activate ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-users"></div>
          </div>
          <span>Menu 2</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Users
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-users"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="users_profile_big ">Sub Menu</a>
              </li>
              <li>
                <a href="users_profile_small ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-edit-32"></div>
          </div>
          <span>Menu 3</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Forms
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-edit-32"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="forms_regular ">Sub Menu</a>
              </li>
              <li>
                <a href="forms_validation ">Sub Menu</a>
              </li>
              <li>
                <a href="forms_wizard ">Sub Menu</a>
              </li>
              <li>
                <a href="forms_uploads ">Sub Menu</a>
              </li>
              <li>
                <a href="forms_wisiwig ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-grid"></div>
          </div>
          <span>Menu 4</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Tables
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-grid"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="tables_regular ">Sub Menu</a>
              </li>
              <li>
                <a href="tables_datatables ">Sub Menu</a>
              </li>
              <li>
                <a href="tables_editable ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
      <li className=" has-sub-menu">
        <a href="#">
          <div className="icon-w">
            <div className="os-icon os-icon-zap"></div>
          </div>
          <span>Menu 5</span></a>
        <div className="sub-menu-w">
          <div className="sub-menu-header">
            Icons
               </div>
          <div className="sub-menu-icon">
            <i className="os-icon os-icon-zap"></i>
          </div>
          <div className="sub-menu-i">
            <ul className="sub-menu">
              <li>
                <a href="icon_fonts_simple_line_icons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_feather ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_themefy ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_picons_thin ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_dripicons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_eightyshades ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="icon_fonts_entypo ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_font_awesome ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_foundation_icon_font ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_metrize_icons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_picons_social ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_batch_icons ">Sub Menu</a>
              </li>
            </ul><ul className="sub-menu">
              <li>
                <a href="icon_fonts_dashicons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_typicons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_weather_icons ">Sub Menu</a>
              </li>
              <li>
                <a href="icon_fonts_light_admin ">Sub Menu</a>
              </li>
            </ul>
          </div>
        </div>
      </li>
    </ul>

  </div>



);

export default MainMenu;