import React from 'react';

const QuickLinks=() =>(

    
    <div className="element-wrapper">
    <h6 className="element-header">
      Quick Links
    </h6>
    <div className="element-box-tp">
      <div className="el-buttons-list full-width">
        <a className="btn btn-white btn-sm" href="#"><i className="os-icon os-icon-delivery-box-2"></i><span>Create New Product</span></a><a className="btn btn-white btn-sm" href="#"><i className="os-icon os-icon-window-content"></i><span>Customer Comments</span></a><a className="btn btn-white btn-sm" href="#"><i className="os-icon os-icon-wallet-loaded"></i><span>Store Settings</span></a>
      </div>
    </div>
  </div>

  
);

export default QuickLinks;