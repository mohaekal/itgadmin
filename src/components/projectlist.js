import React from 'react';

const ProjectList=() =>(

 
<div className="row">
<div className="col-sm-12">
  <div className="element-wrapper">
  
  <table className="table table-responsive table-padded">
                          <thead>
                            <tr>
                              {/* <th></th> */}
                              <th>
                                Assigned Agent
                              </th>
                              <th>
                                Last Comment
                              </th>
                              <th className="text-center">
                                Ticket Category
                              </th>
                              {/* <th>
                                Last Reply Date
                              </th> */}
                              <th>
                                Ticket Status
                              </th>
                              <th>
                                Actions
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar1.jpg")} /><span>John Mayers</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  I have enabled the software for you, you can try now...
                                </div>
                              </td>
                              <td>
                                <span>Today</span><span className="smaller lighter">1:52am</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-success-inverted" href="">Shopping</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller green"></span><span>Active</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar2.jpg")} /><span>Mike Bishop</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  Please approve this request so we can move...
                                </div>
                              </td>
                              <td>
                                <span>Jan 19th</span><span className="smaller lighter">3:22pm</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-danger-inverted" href="">Cafe</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller red"></span><span>Closed</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar3.jpg")} /><span>Terry Crews</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  We will need some login credentials to make...
                                </div>
                              </td>
                              <td>
                                <span>Yesterday</span><span className="smaller lighter">7:45am</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-warning-inverted" href="">Restaurants</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller yellow"></span><span>Pending</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar1.jpg")} /><span>Phil Collins</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  I have enabled the software for you, you can try now...
                                </div>
                              </td>
                              <td>
                                <span>Jan 23rd</span><span className="smaller lighter">2:12pm</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-primary-inverted" href="">Shopping</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller yellow"></span><span>Pending</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar4.jpg")} /><span>Katy Pilsner</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  I have tried this solution but it does not open...
                                </div>
                              </td>
                              <td>
                                <span>Jan 12th</span><span className="smaller lighter">9:51am</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-danger-inverted" href="">Groceries</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller green"></span><span>Active</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                            <tr>
                              {/* <td className="text-center">
                                <input className="form-control" type="checkbox" />
                              </td> */}
                              <td>
                                <div className="user-with-avatar">
                                  <img alt="" src={require(".././img/avatar2.jpg")} /><span>Wes Morgan</span>
                                </div>
                              </td>
                              <td>
                                <div className="smaller lighter">
                                  I have enabled the software for you, you can try now...
                                </div>
                              </td>
                              <td>
                                <span>Jan 9th</span><span className="smaller lighter">12:45pm</span>
                              </td>
                              {/* <td className="text-center">
                                <a className="badge badge-primary-inverted" href="">Business</a>
                              </td> */}
                              <td className="nowrap">
                                <span className="status-pill smaller yellow"></span><span>Pending</span>
                              </td>
                              <td className="row-actions">
                                <a href="#"><i className="os-icon os-icon-grid-10"></i></a><a href="#"><i className="os-icon os-icon-ui-44"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>


    </div>
</div>
</div>

   
  
);

export default ProjectList;