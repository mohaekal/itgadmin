import React from 'react';

import Background1 from '.././img/portfolio9.jpg';
import Background2 from '.././img/portfolio2.jpg';

const AssignTask = () => (



  <div className="row">
    <div className="col-sm-8">
      <div className="element-wrapper compact pt-4">
        <div className="element-actions d-none d-sm-block">
          <a className="btn btn-primary btn-sm" href="#"><i className="os-icon os-icon-ui-22"></i><span>Add Account</span></a><a className="btn btn-success btn-sm" href="#"><i className="os-icon os-icon-grid-10"></i><span>Make Payment</span></a>
        </div>
        <h6 className="element-header">
          Assign Task To
        </h6>
        <div className="element-box-tp">
          <div className="inline-profile-tiles">
            <div className="row">
              <div className="col-4 col-sm-3 col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box faded" href="users_profile_small.html">
                    <div className="pt-new-icon">
                      <i className="os-icon os-icon-plus"></i>
                    </div>
                    <div className="pt-user-name">
                      New<br /> Account
                    </div>
                  </a>
                </div>
              </div>
              <div className="col-4 col-sm-3 col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box" href="users_profile_small.html">
                    <div className="pt-avatar-w">
                      <img alt="" src={require(".././img/avatar1.jpg")} />
                    </div>
                    <div className="pt-user-name">
                      Kelly<br /> Neymayers
                    </div>
                  </a>
                </div>
              </div>
              <div className="col-4 col-sm-3 col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box" href="users_profile_small.html">
                    <div className="pt-avatar-w">
                      <img alt="" src={require(".././img/avatar3.jpg")} />
                    </div>
                    <div className="pt-user-name">
                      Ben<br /> Gossman
                    </div>
                  </a>
                </div>
              </div>
              <div className="col-4 col-sm-3 col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box" href="users_profile_small.html">
                    <div className="pt-avatar-w">
                      <img alt="" src={require(".././img/avatar1.jpg")} />
                    </div>
                    <div className="pt-user-name">
                      Kimson<br /> Broker
                    </div>
                  </a>
                </div>
              </div>
              <div className="col-4 d-sm-none d-xxl-block col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box" href="users_profile_small.html">
                    <div className="pt-avatar-w">
                      <img alt="" src={require(".././img/avatar2.jpg")} />
                    </div>
                    <div className="pt-user-name">
                      Jake<br /> Gilbertson
                    </div>
                  </a>
                </div>
              </div>
              <div className="col-4 d-sm-none d-xxl-block col-xxl-2">
                <div className="profile-tile profile-tile-inlined">
                  <a className="profile-tile-box" href="users_profile_small.html">
                    <div className="pt-avatar-w">
                      <img alt="" src={require(".././img/avatar7.jpg")} />
                    </div>
                    <div className="pt-user-name">
                      Mary<br /> Clintons
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-12 col-xxl-8">
          <div className="element-wrapper compact pt-4">
            <div className="element-actions d-none d-sm-block">
              <form className="form-inline justify-content-sm-end">
                <label className="smaller" for="">News For</label><select className="form-control form-control-sm form-control-faded">
                  <option value="Pending">
                    Bitcoin
                  </option>
                  <option value="Active">
                    Etherium
                  </option>
                  <option value="Cancelled">
                    Litecoin
                  </option>
                </select>
              </form>
            </div>
            <h6 className="element-header">
              Crypto News
            </h6>
            <div className="element-box-tp">
              <div className="post-box">
                <div className="post-media" style={{ backgroundImage: "url(" + Background1 + ")" }}></div>
                <div className="post-content">
                  <h6 className="post-title">
                    Is Crypto the Future of Film Funding?
                  </h6>
                  <div className="post-text">
                    Curiously, view both tone emerged. There should which yards two and concepts amidst liabilities sitting of and, parents it wait
                  </div>
                  <div className="post-foot">
                    <div className="post-tags">
                      <div className="badge badge-primary">
                        BTC
                      </div>
                      <div className="badge badge-primary">
                        Crypto
                      </div>
                    </div>
                    <a className="post-link" href="#"><span>Read Full Story</span><i className="os-icon os-icon-arrow-right7"></i></a>
                  </div>
                </div>
              </div>
              <div className="post-box">
                <div className="post-media" style={{ backgroundImage: "url(" + Background2 + ")" }} ></div>
                <div className="post-content">
                  <h6 className="post-title">
                    Is Crypto the Future of Film Funding?
                  </h6>
                  <div className="post-text">
                    Curiously, view both tone emerged. There should which yards two and concepts amidst liabilities sitting of and, parents it wait
                  </div>
                  <div className="post-foot">
                    <div className="post-tags">
                      <div className="badge badge-primary">
                        BTC
                      </div>
                      <div className="badge badge-primary">
                        Crypto
                      </div>
                    </div>
                    <a className="post-link" href="#"><span>Read Full Story</span><i className="os-icon os-icon-arrow-right7"></i></a>
                  </div>
                </div>
              </div>
              <a className="centered-load-more-link" href="#"><span>Read Our Blog</span></a>
            </div>
          </div>
        </div>
        <div className="col-12 d-sm-none d-xxl-block col-xxl-4">
          <div className="cta-w orange text-center">
            <div className="cta-content extra-padded">
              <div className="highlight-header">
                Bonus
              </div>
              <h5 className="cta-header">
                Invite your friends and make money with referrals
              </h5>
              <form action="">
                <div className="newsletter-field-w">
                  <input placeholder="Email address..." type="text" /><button className="btn btn-sm btn-primary">Send</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-sm-4">
      <div className="element-wrapper compact pt-4">
        <div className="element-actions">
          <form className="form-inline justify-content-sm-end">
            <label className="smaller" for="">Order By</label><select className="form-control form-control-sm form-control-faded">
              <option value="Pending">
                Today
              </option>
              <option value="Active">
                Last Week
              </option>
              <option value="Cancelled">
                Last 30 Days
              </option>
            </select>
          </form>
        </div>
        <h6 className="element-header">
          Transactions
        </h6>
        <div className="element-box-tp">
          <table className="table table-clean">
            <tr>
              <td>
                <div className="value">
                  Amazon Store
                </div>
                <span className="sub-value">Books</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$28.34
                </div>
                <span className="sub-value">12 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Dunkin Donuts
                </div>
                <span className="sub-value">Food & Restaurants</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$7.15
                </div>
                <span className="sub-value">10 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Refund from Sephora
                </div>
                <span className="sub-value">Health & Beauty</span>
              </td>
              <td className="text-right">
                <div className="value text-success">
                  $128.11
                </div>
                <span className="sub-value">10 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Amazon Store
                </div>
                <span className="sub-value">Books</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$28.34
                </div>
                <span className="sub-value">12 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Dunkin Donuts
                </div>
                <span className="sub-value">Food & Restaurants</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$7.15
                </div>
                <span className="sub-value">10 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Refund from Google Store
                </div>
                <span className="sub-value">Health & Beauty</span>
              </td>
              <td className="text-right">
                <div className="value text-success">
                  $15.23
                </div>
                <span className="sub-value">9 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Amazon Store
                </div>
                <span className="sub-value">Books</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$28.34
                </div>
                <span className="sub-value">8 Feb 2018</span>
              </td>
            </tr>
            <tr>
              <td>
                <div className="value">
                  Dunkin Donuts
                </div>
                <span className="sub-value">Food & Restaurants</span>
              </td>
              <td className="text-right">
                <div className="value">
                  -$7.15
                </div>
                <span className="sub-value">8 Feb 2018</span>
              </td>
            </tr>
          </table>
          <a className="centered-load-more-link" href="#"><span>Load More Messages</span></a>
        </div>
      </div>
    </div>
  </div>




      
    );

export default AssignTask;
