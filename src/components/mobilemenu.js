import React from 'react';
import $ from 'jquery';

$(function () {

  // #11. MENU RELATED STUFF

  // INIT MOBILE MENU TRIGGER BUTTON
  $('.mobile-menu-trigger').on('click', function () {
    $('.menu-mobile .menu-and-user').slideToggle(200, 'swing');
    return false;
  });


  // #12. CONTENT SIDE PANEL TOGGLER

  $('.content-panel-toggler, .content-panel-close, .content-panel-open').on('click', function () {
    $('.all-wrapper').toggleClass('content-panel-active');
  });

    // #16. OUR OWN CUSTOM DROPDOWNS 
    $('.os-dropdown-trigger').on('mouseenter', function () {
      $(this).addClass('over');
    });
    $('.os-dropdown-trigger').on('mouseleave', function () {
      $(this).removeClass('over');
    });
  

    var menu_timer;
    $('.menu-activated-on-hover').on('mouseenter', 'ul.main-menu > li.has-sub-menu', function () {
      var $elem = $(this);
      clearTimeout(menu_timer);
      $elem.closest('ul').addClass('has-active').find('> li').removeClass('active');
      $elem.addClass('active');
    });
  
    $('.menu-activated-on-hover').on('mouseleave', 'ul.main-menu > li.has-sub-menu', function () {
      var $elem = $(this);
      menu_timer = setTimeout(function () {
        $elem.removeClass('active').closest('ul').removeClass('has-active');
      }, 30);
    });
  
    // INIT MENU TO ACTIVATE ON CLICK
    $('.menu-activated-on-click').on('click', 'li.has-sub-menu > a', function (event) {
      var $elem = $(this).closest('li');
      if ($elem.hasClass('active')) {
        $elem.removeClass('active');
      } else {
        $elem.closest('ul').find('li.active').removeClass('active');
        $elem.addClass('active');
      }
      return false;
    });
  
});

const MobileMenu=() =>(

<div className="menu-mobile menu-activated-on-click color-scheme-dark">

         <div className="mm-logo-buttons-w">
           <a className="mm-logo" href="#"><img src={require(".././img/logo.png")} /><span>ITG Admin</span></a>
           <div className="mm-buttons">
             <div className="content-panel-open">
               <div className="os-icon os-icon-grid-circles"></div>
             </div>
             <div className="mobile-menu-trigger">
               <div className="os-icon os-icon-hamburger-menu-1"></div>
             </div>
           </div>
         </div>
         <div className="menu-and-user">
           <div className="logged-user-w">
             <div className="avatar-w">
               <img alt="" src={require(".././img/avatar1.jpg")} />
             </div>
             <div className="logged-user-info-w">
               <div className="logged-user-name">
                Mohamad Haekal
               </div>
               <div className="logged-user-role">
                 Administrator
               </div>
             </div>
           </div>

           <ul className="main-menu">
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-layout"></div>
                 </div>
                 <span>Menu 1</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="#">Menu 1</a>
                 </li>
                 <li>
                   <a href="#">Menu 2 <strong className="badge badge-danger">Hot</strong></a>
                 </li>
                 <li>
                   <a href="#">Menu 3</a>
                 </li>
                 <li>
                   <a href="#">Menu 4</a>
                 </li>
                 <li>
                   <a href="#">Menu 5</a>
                 </li>
                 <li>
                   <a href="#">Menu 6</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-layers"></div>
                 </div>
                 <span>Menu 2</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu <strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="#">Side &amp; Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Side &amp; Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu <strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-package"></div>
                 </div>
                 <span>Menu 3</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu</a>
                 </li>
                 <li>
                   <a href="#">Sub Menu <strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="apps_projects.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="apps_bank.html">Sub Menu<strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="apps_full_chat.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="apps_todo.html">Sub Menu<strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="misc_chat.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="apps_pipeline.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="rentals_index_grid.html">Sub Menu<strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="misc_calendar.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-file-text"></div>
                 </div>
                 <span>Menu 4</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="misc_invoice.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="rentals_index_grid.html">Sub Menu<strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="misc_charts.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="auth_login.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="auth_register.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="auth_lock.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="misc_pricing_plans.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="misc_error_404.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="misc_error_500.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-life-buoy"></div>
                 </div>
                 <span>Menu 5</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="uikit_modals.html">Sub Menu<strong className="badge badge-danger">New</strong></a>
                 </li>
                 <li>
                   <a href="uikit_alerts.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_grid.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_progress.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_popovers.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_tooltips.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_buttons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_dropdowns.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="uikit_typography.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-mail"></div>
                 </div>
                 <span>Menu 6</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="emails_welcome.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="emails_order.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="emails_payment_due.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="emails_forgot.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="emails_activate.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-users"></div>
                 </div>
                 <span>Menu 7</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="users_profile_big.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="users_profile_small.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-edit-32"></div>
                 </div>
                 <span>Menu 8</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="forms_regular.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="forms_validation.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="forms_wizard.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="forms_uploads.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="forms_wisiwig.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-grid"></div>
                 </div>
                 <span>Menu 9</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="tables_regular.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="tables_datatables.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="tables_editable.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
             <li className="has-sub-menu">
               <a href="#">
                 <div className="icon-w">
                   <div className="os-icon os-icon-zap"></div>
                 </div>
                 <span>Menu 10</span></a>
               <ul className="sub-menu">
                 <li>
                   <a href="icon_fonts_simple_line_icons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_feather.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_themefy.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_picons_thin.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_dripicons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_eightyshades.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_entypo.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_font_awesome.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_foundation_icon_font.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_metrize_icons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_picons_social.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_batch_icons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_dashicons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_typicons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_weather_icons.html">Sub Menu</a>
                 </li>
                 <li>
                   <a href="icon_fonts_light_admin.html">Sub Menu</a>
                 </li>
               </ul>
             </li>
           </ul>

           </div>
      
         </div>

  
);

export default MobileMenu;