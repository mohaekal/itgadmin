import React from 'react';

const ContentPanelToogler=() =>(

    <div className="content-panel-toggler">
    <i className="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
  </div>

  
);

export default ContentPanelToogler;