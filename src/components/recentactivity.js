import React from 'react';

const RecentActivity=() =>(

 
    <div className="element-wrapper">
    <h6 className="element-header">
      Recent Activity
    </h6>
    <div className="element-box-tp">
      <div className="activity-boxes-w">
        <div className="activity-box-w">
          <div className="activity-time">
            10 Min
          </div>
          <div className="activity-box">
            <div className="activity-avatar">
              <img alt="" src={require(".././img/avatar1.jpg")} />
            </div>
            <div className="activity-info">
              <div className="activity-role">
                John Mayers
              </div>
              <strong className="activity-title">Opened New Account</strong>
            </div>
          </div>
        </div>
        <div className="activity-box-w">
          <div className="activity-time">
            2 Hours
          </div>
          <div className="activity-box">
            <div className="activity-avatar">
              <img alt="" src={require(".././img/avatar2.jpg")} />
            </div>
            <div className="activity-info">
              <div className="activity-role">
                Ben Gossman
              </div>
              <strong className="activity-title">Posted Comment</strong>
            </div>
          </div>
        </div>
        <div className="activity-box-w">
          <div className="activity-time">
            5 Hours
          </div>
          <div className="activity-box">
            <div className="activity-avatar">
              <img alt="" src={require(".././img/avatar3.jpg")} />
            </div>
            <div className="activity-info">
              <div className="activity-role">
                Phil Nokorin
              </div>
              <strong className="activity-title">Opened New Account</strong>
            </div>
          </div>
        </div>
        <div className="activity-box-w">
          <div className="activity-time">
            2 Days
          </div>
          <div className="activity-box">
            <div className="activity-avatar">
              <img alt="" src={require(".././img/avatar4.jpg")} />
            </div>
            <div className="activity-info">
              <div className="activity-role">
                Jenny Miksa
              </div>
              <strong className="activity-title">Uploaded Image</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  
);

export default RecentActivity;