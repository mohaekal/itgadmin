import React from 'react';
import ItgMainContent from './maincontent';

const MainItg = () => (

  <div className="menu-position-side menu-side-left full-screen with-content-panel" >
    <div className="all-wrapper with-side-panel solid-bg-all">
      <ItgMainContent />
    </div>
  </div>

);

export default MainItg;