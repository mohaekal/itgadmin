import React from 'react';

const RecentOrder=() =>(

 
    <div className="row">
    <div className="col-sm-12">
      <div className="element-wrapper">
        <h6 className="element-header">
          Recent Orders
        </h6>
        <div className="element-box-tp">
          {/* <!--------------------
          START - Controls Above Table
          --------------------> */}
          <div className="controls-above-table">
            <div className="row">
              <div className="col-sm-6">
                <a className="btn btn-sm btn-secondary" href="#">Download CSV</a><a className="btn btn-sm btn-secondary" href="#">Archive</a><a className="btn btn-sm btn-danger" href="#">Delete</a>
              </div>
              <div className="col-sm-6">
                <form className="form-inline justify-content-sm-end">
                  <input className="form-control form-control-sm rounded bright" placeholder="Search" type="text" /><select className="form-control form-control-sm rounded bright">
                    <option selected="selected" value="">
                      Select Status
                    </option>
                    <option value="Pending">
                      Pending
                    </option>
                    <option value="Active">
                      Active
                    </option>
                    <option value="Cancelled">
                      Cancelled
                    </option>
                  </select>
                </form>
              </div>
            </div>
          </div>
          {/* <!--------------------
          END - Controls Above Table
          ------------------          --><!--------------------
          START - Table with actions
          ------------------  --> */}
          <div className="table-responsive">
            <table className="table table-bordered table-lg table-v2 table-striped">
              <thead>
                <tr>
                  <th className="text-center">
                    <input className="form-control" type="checkbox" />
                  </th>
                  <th>
                    Customer Name
                  </th>
                  <th>
                    Country
                  </th>
                  <th>
                    Order Total
                  </th>
                  <th>
                    Referral
                  </th>
                  <th>
                    Status
                  </th>
                  <th>
                    Actions
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="text-center">
                    <input className="form-control" type="checkbox" />
                  </td>
                  <td>
                    John Mayers
                  </td>
                  <td>
                    <img alt="" src={require(".././img/flags-icons/us.png")} width="25px" />
                  </td>
                  <td className="text-right">
                    $245
                  </td>
                  <td>
                    Organic
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="row-actions">
                    <a href="#"><i className="os-icon os-icon-ui-49"></i></a><a href="#"><i className="os-icon os-icon-grid-10"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                  </td>
                </tr>
                <tr>
                  <td className="text-center">
                    <input className="form-control" type="checkbox" />
                  </td>
                  <td>
                    Mike Astone
                  </td>
                  <td>
                    <img alt="" src={require(".././img/flags-icons/fr.png")} width="25px" />
                  </td>
                  <td className="text-right">
                    $154
                  </td>
                  <td>
                    Organic
                  </td>
                  <td className="text-center">
                    <div className="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                  </td>
                  <td className="row-actions">
                    <a href="#"><i className="os-icon os-icon-ui-49"></i></a><a href="#"><i className="os-icon os-icon-grid-10"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                  </td>
                </tr>
                <tr>
                  <td className="text-center">
                    <input className="form-control" type="checkbox" />
                  </td>
                  <td>
                    Kira Knight
                  </td>
                  <td>
                    <img alt="" src={require(".././img/flags-icons/us.png")} width="25px" />
                  </td>
                  <td className="text-right">
                    $23
                  </td>
                  <td>
                    Adwords
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="row-actions">
                    <a href="#"><i className="os-icon os-icon-ui-49"></i></a><a href="#"><i className="os-icon os-icon-grid-10"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                  </td>
                </tr>
                <tr>
                  <td className="text-center">
                    <input className="form-control" type="checkbox" />
                  </td>
                  <td>
                    Jessica Bloom
                  </td>
                  <td>
                    <img alt="" src={require(".././img/flags-icons/ca.png")}  width="25px"/>
                  </td>
                  <td className="text-right">
                    $112
                  </td>
                  <td>
                    Organic
                  </td>
                  <td className="text-center">
                    <div className="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                  </td>
                  <td className="row-actions">
                    <a href="#"><i className="os-icon os-icon-ui-49"></i></a><a href="#"><i className="os-icon os-icon-grid-10"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                  </td>
                </tr>
                <tr>
                  <td className="text-center">
                    <input className="form-control" type="checkbox" />
                  </td>
                  <td>
                    Gary Lineker
                  </td>
                  <td>
                    <img alt="" src={require(".././img/flags-icons/ca.png")} width="25px" />
                  </td>
                  <td className="text-right">
                    $64
                  </td>
                  <td>
                    Organic
                  </td>
                  <td className="text-center">
                    <div className="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                  </td>
                  <td className="row-actions">
                    <a href="#"><i className="os-icon os-icon-ui-49"></i></a><a href="#"><i className="os-icon os-icon-grid-10"></i></a><a className="danger" href="#"><i className="os-icon os-icon-ui-15"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {/* <!--------------------
          END - Table with actions
          ------------------            --><!--------------------
          START - Controls below table
          ------------------  --> */}
          <div className="controls-below-table">
            <div className="table-records-info">
              Showing records 1 - 5
            </div>
            <div className="table-records-pages">
              <ul>
                <li>
                  <a href="#">Previous</a>
                </li>
                <li>
                  <a className="current" href="#">1</a>
                </li>
                <li>
                  <a href="#">2</a>
                </li>
                <li>
                  <a href="#">3</a>
                </li>
                <li>
                  <a href="#">4</a>
                </li>
                <li>
                  <a href="#">Next</a>
                </li>
              </ul>
            </div>
          </div>
          {/* <!--------------------
          END - Controls below table
          --------------------> */}
        </div>
      </div>
    </div>
  </div>



);

export default RecentOrder;